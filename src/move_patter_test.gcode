G28 ;Home All axis
G01 Z20.5 ; Rise to avoid bed
G01 X41 Y16 ; Go to starting point
G01 Z35 ;Rise to avoid Containers
G01 X71 Y140 ;Go to first Container
G01 Z22 ;Drop to Container
G01 Z35 ;Rise from Container
G01 X71 Y40 ; Move to first Dot point
G01 Z20.3 ;Drop and Create dot
G01 Z50 ;Rise to avoid Bath
G01 X180 Y40 ;Go to first bath point
G01 Z29 ;Wash
G01 X210 
G01 X180 
G01 X220 
G01 X180 
G01 Y60 X210 
G01 X180
G01 Z50 ;Rise to avoid bath 
G01 X109 Y140 ;Go to second container
G01 Z22 ;Drop to take sample
G01 Z35 ;Rise to avoid Container
G01 X71.5 Y40 ;Move to create second dot 
G01 Z20.3 ;Drop and create dot
G01 Z50 ;Rise to avoid everything
G28 ;Home Al axis