---


<div align="center">
<p>
    <a href="https://benchling.com/organizations/acubesat/">Benchling 🎐🧬</a> &bull;
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_PL.pdf?expanded=true&viewer=rich">DDJF_PL 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>

## Description

The microfluidic design used in AcubeSAT necessitates placing the yeast spores at pre-determined X-Y coordinates on top of an epoxy-coated glass slide, in a process called **spotting**.

This is commonly done by repurposed DNA microarray robots. Normally, they look like [this](https://shop.unigreenscheme.co.uk/other-lab-equipment/genetix-qarray-2-microarray-spotter-plant-cloning-unit-lab-2ixtt). Some other, smaller [alternatives](http://labnext.com/xactii.html) are also commercially available.

Since a high capacity is not needed, repurposing a 3D-printer with good precision (e.g. <500 um) may do the trick.

## Required Tools

 Hardware

- Creality Ender 3 or Creality CR10 (Can be adapted for other printers as well)

 Software

- G-Code Uploader. This project uses [Pronterface](https://github.com/kliment/Printrun).
- (Optional) If you need a G-Code editor/highlighter, VsCode with nc-gcode plugin can be used.
